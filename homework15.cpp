
#include <iostream>
#include <string>

int main()
{
    std::cout << "Hello, teacher! \n \n";
    int quant = 0;
    int count = 0;
    std::string OrOr;
    
        
    std::cout << "Set number: ";
    std::cin >> quant; "\n";
    std::cout << "Show [odd] or [even] numbers?: ";
    std::cin >> OrOr;

    while (count < quant)
    {
        count++;
        if (count % 2 == 0)
        {
            if (OrOr == "even")
            {
                std::cout << count << " - is even\n";
            }
        }
        else
        {
            if (OrOr == "odd")
            {
                std::cout << count << " - is odd\n";
            }
        }
        
    }
    std::cout << "\n Bye, bye! Have a great time! \n";
    return 0;
}
